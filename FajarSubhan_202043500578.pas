program fajarsubhan_578;
uses crt;
type
	data 
		= record
			name 		: String[20];
			count 		: integer; 
			total 		: real;
			discount	: real;
			cuting  	: real; 
			tot_price 	: real;
	end;
var
	i,n 	: Integer;
	value 	: array[1..10] of data;
begin
clrscr;
	write('Masukkan Jumlah Pembeli : ') ;readln(n);
		
	writeln();
	for i := 1 to n do
	begin
		writeln('Masukkan Data Ke        ',i);
		
		write('Nama        : '); 
		readln(value[i].name);
		
		write('Banyaknya   : '); 
		readln(value[i].count);

		{
			- Harga 1pcs payung = 40.000 
			- Jumlah Harga = jumlah barang * harga satuan 
		}
		value[i].total:= 40000*value[i].count;
	
		{
			- Jika Jumlah harga >= 300.000 maka discount 15%
		}
		if (value[i].total >= 300000) then
		begin
            value[i].discount:=15/100;
        end
		
		{
			- Jika Jumlah harga <= 300.000 maka tidak ada discount 
		}
		else if (value[i].total <= 300000) then
        begin
            value[i].discount:=0;
        end;

		value[i].cuting 	:= value[i].total*value[i].discount;
		value[i].tot_price 	:= value[i].total-value[i].cuting;
	end;


WriteLn('=====================================================');
WriteLn('|No| Nama | Banyaknya | Jumlah Harga | Discount | Total Bayar |');
WriteLn('=====================================================');
for i := 1 to n do
WriteLn('|',i,' | ',value[i].name:3,' |  ',value[i].count:3,'  |  ',value[i].total:3:0,'| ',value[i].cuting:3:0,'         |       ',value[i].tot_price:3:0,' |');
WriteLn('================================================');
end.